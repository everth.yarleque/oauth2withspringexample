package com.example.oauth;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

import java.security.Principal;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

  @GetMapping("/")
  public ResponseEntity<String> hello() {
    return status(OK).body("Hello World");
  }

  @GetMapping("/user")
  public Principal principal(Principal principal) {
    System.out.println(principal.getName());
    return principal;
  }
}
