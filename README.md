# Oauth2
Debemos crear nuestras credenciales de Google en la consola, podemos hacer en la siguiente url: https://console.cloud.google.com/?hl=es 
![alt text](https://enfoquenomada.com/wp-content/uploads/2016/01/Nuevas-Credenciales-Google-Client-ID.jpg)

Luego en Authorized redirect URIs ![alt tex](https://ticksy_attachments.s3.amazonaws.com/5770786011.png)

agregaremos lo siguiente: http://localhost:8080/login/oauth2/code/google y guardamos para obtener nuestras credenciales

En application.properties agregaremos lo siguiente:
spring.security.oauth2.client.registration.google.client-id=<YOUR_CLIENT_ID>
spring.security.oauth2.client.registration.google.client-secret=<YOUR_CLIENT_SECRET>

En la clase Controller tenemos dos metodos sencillos, el primero nos devolverá "Hello World" y el segundo nos devolverá la informacion del usuario que se autenticó

ejecutamos el proyecto e ingresamos al navegador y colocamos http://localhost:8080 y http://localhost:8080/user, nos autenticamos y deberá devolvernos la informacion
